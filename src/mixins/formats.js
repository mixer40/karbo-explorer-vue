export default{

    data: function () {
        return {
            coinUnits: 1000000000000,
            symbol: 'KRB',
            blockTargetInterval :240

        }
    },
    methods: {
        getReadableHashRateString: function (hashrate) {
            var i = 0;
            var byteUnits = [' H', ' kH', ' MH', ' GH', ' TH', ' PH', ' EH', ' ZH', ' YH'];
            while (hashrate > 1000) {
                hashrate = hashrate / 1000;
                i++;
            }
            return hashrate.toFixed(2) + byteUnits[i];
        },

        getReadableDifficultyString: function (difficulty, precision) {
            if (isNaN(parseFloat(difficulty)) || !isFinite(difficulty)) return 0;
            if (typeof precision === 'undefined') precision = 2;
            var units = ['', 'k', 'M', 'G', 'T', 'P'],
                number = Math.floor(Math.log(difficulty) / Math.log(1000));
            if (units[number] === undefined || units[number] === null) {
                return 0
            }
            return (difficulty / Math.pow(1000, Math.floor(number))).toFixed(precision) + ' ' + units[number];
        },

        getReadableCoins: function (coins, digits, withoutSymbol) {
            var amount = (parseInt(coins || 0) / this.coinUnits).toFixed(digits || this.coinUnits.toString().length - 1);
            return amount + (withoutSymbol ? '' : (' ' + this.symbol));
        },
        formatDate: function (time) {
            if (!time) return '';
            return new Date(parseInt(time) * 1000).toLocaleString();
        },

        formatBytes: function (a, b) {
            if (0 == a) return "0 Bytes"; var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], f = Math.floor(Math.log(a) / Math.log(c)); return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f]
        },


    }
}