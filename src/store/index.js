import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

Vue.use(Vuex)

const API ="https://karbo-exchange.com.ua/nodemonitor/getKarboJsonRPC.php";

export default new Vuex.Store({
    state: {
        lastBlock: {},
        nodeInfo: {},
        lastBlockHash: {},
        blocks: [],
        transactionspool: []
    },
    getters: {
        avgSolveTime: (state) => {

            let avg_solve_time = 0;
            let solveTime = 0;
            for (let i = 1; i < state.blocks.length; i++) {
                solveTime += Math.abs(state.blocks[i].timestamp - state.blocks[i - 1].timestamp);
            }
            avg_solve_time = solveTime / (state.blocks.length - 1);

            return Math.abs(avg_solve_time);
        },
        avgDiff: (state) => {


            if (state.blocks.length) {

                return state.blocks.reduce((a, b) => a + b.difficulty, 0) / state.blocks.length;
            } else
                return 11;
        }
    },
    actions: {
        getTransactionByHash(context, _hash) {
            return axios.get(API + "?gettransaction=1&hash=" + _hash)
        },
        
        getBlockheaderbyHeight(context, height){
            return axios.get(API+"?getblockheaderbyheight=1&height=" + height)
        },
        getLastBlock(context) {

            axios
                .get(API+"?getlastblockheader")
                .then(response => {
                    context.commit('setLastBlockHash', response.data.result.block_header.hash);
                    axios.get(API+"?f_block_json=1&hash=" + response.data.result.block_header.hash).then(response => {
                        context.commit('setLastBlockInfo', response.data.result.block);
                    });

                })
        },
        getInfo(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get(API+"?getinfo")
                    .then(response => {
                        context.commit('setNodeInfo', response.data);
                        axios.get(API+"?f_blocks_list_json=1&height=" + (response.data.height - 1))
                            .then(response => {
                                context.commit('setBlocks', response.data.result.blocks);
                            });
                        
                        axios.get(API + "?gettransactionspool=1").then(response => {
                            context.commit('setTransactionsPool', response.data.result.transactions);
                        });
                        resolve(response.data)
                    }).catch(function () {
                        reject({})
                    })
            })
        }, 
        getInfoApdate(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get(API + "?getinfo")
                    .then(response => {
                        context.commit('setNodeInfo', response.data);
                        let currentHeight = response.data.height;
                        
  
                        axios.get(API + "?f_blocks_list_json=1&height=" + (response.data.height - 1))
                            .then(response => {
                                
                                let maxHeight = this.state.blocks.reduce((accumulator, currentValue) =>
                                    accumulator > currentValue.height ? accumulator : currentValue.height
                                );
                                let diff = currentHeight - maxHeight -1;
                                if (diff>0){
                                    
                                    context.commit('appendBeginBlocks', response.data.result.blocks.filter(item => item.height > maxHeight+1));
                                }
                            });

                        axios.get(API + "?gettransactionspool=1").then(response => {
                            context.commit('setTransactionsPool', response.data.result.transactions);
                        });
                        resolve(response.data)
                    }).catch(function () {
                        reject({})
                    })
            })
        },
        loadByHash(context, _hash) {
            return axios.get(API+"?f_block_json=1&hash=" + _hash)
        },
        loadBlocksByHeight(context, _height) {
            axios.get(API+"?f_blocks_list_json=1&height=" + _height)
                .then(response => {
                    context.commit('appendBlocks', response.data.result.blocks);
                });
        }
    },
    mutations: {
        appendBeginBlocks(state, blocks) {
            state.blocks = blocks.concat(state.blocks);
        },
        appendBlocks(state, blocks) {
            state.blocks = state.blocks.concat(blocks);
        },
        setBlocks(state, blocks) {
            state.blocks = blocks;
        },
        setTransactionsPool(state, transactions) {
            state.transactionspool = transactions;

        },
        setLastBlockInfo(state, lastBlock) {
            state.lastBlock = lastBlock;
        },

        setLastBlockHash(state, lastBlockHash) {
            state.lastBlockHash = lastBlockHash;
        },
        setNodeInfo(state, nodeInfo) {
            state.nodeInfo = nodeInfo;
        }
    }
})